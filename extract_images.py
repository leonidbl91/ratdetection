import cv2
import glob

CALIBRATION_FRAME_POSITION = 120

films = glob.glob('*.MP4')

for film in films:
	cam = cv2.VideoCapture(film)
	cam.set(cv2.CAP_PROP_POS_FRAMES, CALIBRATION_FRAME_POSITION)
	(ret, frame) = cam.read()
	cv2.imwrite(film + '.frame.jpg', frame)
	
