
from mpl_toolkits.mplot3d import Axes3D

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import cv2

import json

help_message='''
Triangulate coordinates in 3D space.
USAGE: triangulation.py [--calibration <calibration-dir>] 
                         <camera1-coords> <camera2-coords>

<camera1-coords> - Camera 1 coordinates file.
<camera2-coords> - Camera 2 coordinates file.
<calibration-dir> - Dir with calibration data files (stereo.calibration.*).

'''

def triangulate_data(P1, P2, a, b):
    X = cv2.triangulatePoints( P1, P2, a, b)
    # Remember to divide out the 4th row. Make it homogeneous
    X /= X[3]
    return X[:3]


def draw_track(X):
    fig = plt.figure()
    ax = Axes3D(fig)
    ax.plot(X[0], X[1], X[2], label='parametric curve')
    plt.show()

def store_track(X, filename):
    with open(filename, "w") as f:
        json.dump(X.tolist(), f)

def get_coords (filename):
    try:
        fp = open (filename)
    except:
        return (None)

    coords = []
    for line in fp.readlines ():
        line_elems = line.split ()
        if (line_elems [0] != 'detected:'):
            continue

        (msg, frame, x, y, width, height) = line_elems
        frame = ''.join ([c for c in frame if c.isdigit ()])
        x = ''.join ([c for c in x if c.isdigit ()])
        y = ''.join ([c for c in y if c.isdigit ()])
        width = ''.join ([c for c in width if c.isdigit ()])
        height = ''.join ([c for c in height if c.isdigit ()])
        coords.append ([int(frame), int(x), int(y), int(width), int(height)])

    return (coords)

def get_matching_coords (coords1, coords2):
    max_val1 = max ((coord[0] for coord in coords1))
    max_val2 = max ((coord[0] for coord in coords2))
    max_val = max ((max_val1, max_val2))
    print (max_val+1)
    sieve = [0 for _ in range (max_val+1)]
    for coord in coords1:
        sieve [coord [0]] = sieve [coord [0]] + 1

    for coord in coords2:
        sieve [coord [0]] = sieve [coord [0]] + 1

    return ([index for index, val in enumerate(sieve) if (val == 2)],
            [coord for coord in coords1 if (sieve [coord [0]] == 2)],
            [coord for coord in coords2 if (sieve [coord [0]] == 2)])

# my data. TODO: replace
P1 = np.eye(4)
P2 = np.array([[ 0.878, -0.01 ,  0.479, -1.995],
            [ 0.01 ,  1.   ,  0.002, -0.226],
            [-0.479,  0.002,  0.878,  0.615],
            [ 0.   ,  0.   ,  0.   ,  1.   ]])

# Homogeneous arrays
a3xN = np.array([[ 0.091,  0.167,  0.231,  0.083,  0.154],
              [ 0.364,  0.333,  0.308,  0.333,  0.308],
              [ 1.   ,  1.   ,  1.   ,  1.   ,  1.   ]])
b3xN = np.array([[ 0.42 ,  0.537,  0.645,  0.431,  0.538],
              [ 0.389,  0.375,  0.362,  0.357,  0.345],
              [ 1.   ,  1.   ,  1.   ,  1.   ,  1.   ]])

a = a3xN[:2]
b = b3xN[:2]
P1 = P1[:3]
P2 = P2[:3]

if __name__ == "__main__":
    import sys
    import getopt

    print help_message

    dir = '.'
    opts, args = getopt.getopt (sys.argv [1:], 'c:', [ 'calibration=' ])
    for opt, arg in opts:
        if (opt in ['-c', '--calibration']):
            dir = arg
            continue
        print ('Unknown option %s.' % opt)
        exit (1)

    try:
        cam1_file = args [0]
        cam2_file = args [1]
    except:
        print 'Missing camera coordinates files.'
        exit (1)
    #
    # Load calibration data.
    #
    R = np.loadtxt (dir + '/stereo.calibration.rotation.txt')
    T = np.loadtxt (dir + '/stereo.calibration.translation.txt')
    cam1_matrix = np.loadtxt (dir + '/stereo.calibration.cam1.mat.txt')
    cam1_coeffs = np.loadtxt (dir + '/stereo.calibration.cam1.coef.txt')
    cam2_matrix = np.loadtxt (dir + '/stereo.calibration.cam2.mat.txt')
    cam2_coeffs = np.loadtxt (dir + '/stereo.calibration.cam2.coef.txt')
    imgsize = np.loadtxt (dir + '/stereo.calibration.cam2.imgsize.txt')
    imgsize = tuple (val for val in imgsize.astype (np.int))
    #
    # Compute camera projection matrixes.
    #
    (R1, R2, P1, P2, Q, roi1, roi1) = \
            cv2.stereoRectify (cam1_matrix, cam1_coeffs,
                               cam2_matrix, cam2_coeffs,
                               imgsize, R,  T)
    print (P1, P2)
    #
    # Load coordinates.
    #
    coords1 = get_coords (cam1_file)
    if (coords1 is None):
        exit (1)

    coords2 = get_coords (cam2_file)
    if (coords2 is None):
        exit (1)

    (frames, common1, common2) = get_matching_coords (coords1, coords2)

    a = np.asarray ([ [(coord [0] + coord [2] / 2) for coord in common1],
                      [(coord [1] + coord [3] / 2) for coord in common1] ],
                    dtype = np.float)
    b = np.asarray ([ [(coord [0] + coord [2] / 2) for coord in common2],
                      [(coord [1] + coord [3] / 2) for coord in common2] ],
                    dtype = np.float)
    X = triangulate_data (P1, P2, a, b)
    print (a)
    print (b)
    print (X)
    draw_track(X)
    store_track(X, "out.json")
