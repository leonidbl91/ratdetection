#! /usr/bin/env python

import cv2
import math
import numpy

CHESS_GEOMETRY = (6,9)
CHESS_CELL_EDGE_LENGTH = 2.5

help_message='''
USAGE: stereo_calibrate.py [calibration photos dir]
Computes the stereo camera calibration parameters.

<calibration photos dir> - Directory with photos generated by 
                           extract_calibration.py
'''

def get_chessboard_objpoints (size, stride):
    x = numpy.asarray (range (1, size [0] + 1),
                       dtype = numpy.float).repeat (size [1])
    y = numpy.asarray (range (1, size [1] + 1),
                       dtype = numpy.float).repeat (size [0])
    y = y.reshape (size [1], size [0]).transpose ().flatten ()
    z = numpy.zeros (size [0] * size [1])

    points = x
    points = numpy.append (points, y)
    points = numpy.append (points, z)
    points = points.reshape (3, size [0], size [1]).transpose ()
    points = points.reshape (-1, 3) * stride
    return (points)

def disp_images (frame1, frame2, message, delay):
    cv2.putText (frame1, message, (30, 30),
                 cv2.FONT_HERSHEY_DUPLEX, 1, (0, 0, 255))
    cv2.putText (frame2, message, (30, 30),
                 cv2.FONT_HERSHEY_DUPLEX, 1, (0, 0, 255))
    cv2.imshow ('Camera1', frame1)
    cv2.imshow ('Camera2', frame2)
    cv2.waitKey (delay)

if __name__ == '__main__':
    import glob
    import sys

    print help_message
    try:
        dir = sys.argv [1]
    except:
        print 'no video directory'
        exit(1)

    cam1_photos = sorted (glob.glob ('%s/calib-cam%d-*.npy' % (dir, 1)))
    cam2_photos = sorted (glob.glob ('%s/calib-cam%d-*.npy' % (dir, 2)))

    assert len (cam1_photos) == len (cam2_photos), str (
        '# of photos for each camera does not match (%d != %d)' %
        (len (cam1_photos), len (cam2_photos)))

    chess_points = get_chessboard_objpoints (CHESS_GEOMETRY, CHESS_CELL_EDGE_LENGTH)
    cam1_points = [ ]
    cam2_points = [ ]
    obj_points  = [ ]

#    for index in range (len (cam1_photos)):
    for index in range (len (cam1_photos)):
        #
        # Get photos
        #
        frame1 = numpy.load (cam1_photos [index])
        frame2 = numpy.load (cam2_photos [index])
        #
        # Find chessboard corners
        #
        frame_gray = cv2.cvtColor (frame1, cv2.COLOR_BGR2GRAY)
        ret1, corners1 = \
            cv2.findChessboardCorners (frame_gray,  CHESS_GEOMETRY,
                                       flags = cv2.CALIB_CB_ADAPTIVE_THRESH)
        if (not ret1):
            disp_images(frame1, frame2, 'Cam1 chessboard not found.', 10000)
            continue

        frame_gray = cv2.cvtColor (frame2, cv2.COLOR_BGR2GRAY)
        ret2, corners2 = \
            cv2.findChessboardCorners (frame_gray,  CHESS_GEOMETRY,
                                       flags = cv2.CALIB_CB_ADAPTIVE_THRESH)
        if (not ret2):
            disp_images(frame1, frame2, 'Cam2 chessboard not found.', 10000)
            continue
        #
        # Draw chessboard corners for display
        #
        frame1 = cv2.drawChessboardCorners (frame1, CHESS_GEOMETRY, corners1, ret1)
        frame2 = cv2.drawChessboardCorners (frame2, CHESS_GEOMETRY, corners2, ret2)
        disp_images (frame1, frame2, 'Corners found.', 1000)
        #
        # Append points to camera points.
        #
        cam1_points.append (corners1)
        cam2_points.append (corners2)
        obj_points.append (chess_points)
    #
    # Calibrate camera.
    #
    cv2.destroyAllWindows ()
    cv2.waitKey (1)
    print ('Preparing calibration parameters.')
    cam1_points = numpy.asarray (cam1_points)
    #print (cam1_points.shape)
    #print (cam1_points)
    cam2_points = numpy.asarray (cam2_points)
    #print (cam2_points.shape)
    #print (cam2_points)
    #print (type (obj_points))
    obj_points  = numpy.asarray (obj_points,
                  dtype=numpy.float32)
    #print (obj_points.shape)
    #print (obj_points)
    cam1matrix = numpy.eye (3)
    cam1co = numpy.zeros (4)
    cam2matrix = numpy.eye (3)
    cam2co = numpy.zeros (4)
    imgsize = (frame1.shape [0], frame1.shape [1])
    rc, cam1matrix, cam1co, cam2matrix, cam2co, R, T, E, F = \
        cv2.stereoCalibrate (obj_points, cam1_points, cam2_points,
                             cam1matrix, cam1co,
                             cam2matrix, cam2co, imgsize,
                             criteria = (cv2.TERM_CRITERIA_MAX_ITER+
                                         cv2.TERM_CRITERIA_EPS,
                                         100, 1e-5))
    print ('Rotation Matrix')
    print (R)
    print ('Translation Vector')
    print (T)
    print ('Camera Matrices (fixed for now)')
    print (cam1matrix)
    print (cam2matrix)
    print ('Camera Coefficients (fixed for now)')
    print (cam1co)
    print (cam2co)

    numpy.savetxt('stereo.calibration.rotation.txt', R)
    numpy.savetxt('stereo.calibration.translation.txt', T)
    numpy.savetxt('stereo.calibration.cam1.mat.txt', cam1matrix)
    numpy.savetxt('stereo.calibration.cam1.coef.txt', cam1co)
    numpy.savetxt('stereo.calibration.cam2.mat.txt', cam2matrix)
    numpy.savetxt('stereo.calibration.cam2.coef.txt', cam2co)
    numpy.savetxt('stereo.calibration.cam2.imgsize.txt', imgsize)
    
