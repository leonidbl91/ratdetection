import numpy as np
import cv2
import sys
import time
import glob


# termination criteria
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)


def find_chessboard(objp, imgpoints, objpoints, frame):
	# prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
	img = frame
	gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

	# Find the chess board corners
	ret, corners = cv2.findChessboardCorners(gray, (6,9), flags = cv2.CALIB_CB_ADAPTIVE_THRESH)

	# If found, add object points, image points (after refining them)
	if ret == True:
		objpoints.append(objp)

		corners2 = cv2.cornerSubPix(gray,corners,(11,11),(-1,-1),criteria) 
		imgpoints.append(corners2)

		# Draw and display the corners
		img = cv2.drawChessboardCorners(img, (6,9), corners2,ret)
		cv2.imshow('preview',img)
	else:
		print 'Cant find calibration corners!!'
		sys.exit(1)

def calculate_cam_matrix(imgpoints, img, objpoints):
	gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
	ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1],None,None)
	return mtx, dist

def load_image(filename):
	frame = cv2.imread(filename)
	return frame

def calibrate_camera(directory):
	objp = np.zeros((9*6,3), np.float32)
	objp[:,:2] = np.mgrid[0:6,0:9].T.reshape(-1,2)
	images = sorted(glob.glob(directory + '/*.jpg'))

	imgpoints = [] # 2d points in image plane.
	# Arrays to store object points and image points from all the images.
	objpoints = [] # 3d point in real world space

	frame_size = 0
	for img in images:
		print img
		frame = load_image(img)
		frame_size = frame.shape
		find_chessboard(objp, imgpoints, objpoints, frame)
	
	(mtx, dist) = calculate_cam_matrix(imgpoints, frame, objpoints)
	
	return mtx, dist, objpoints, imgpoints, frame_size[0:2]

camera1ImagesDirectory = sys.argv[1]
camera2ImagesDirectory = sys.argv[2]

(camera1mtx, camera1dist, camera1objpoints, camera1imgpoints, camera1framesize) = calibrate_camera(camera1ImagesDirectory)

print 'camera 1'
print camera1mtx
print '-----------'
print camera1dist
print '-----------'
print camera1framesize

(camera2mtx, camera2dist, camera2objpoints, camera2imgpoints, camera2framesize) = calibrate_camera(camera2ImagesDirectory)

print 'camera 2'
print camera2mtx
print '-----------'
print camera2dist
print '-----------'
print camera2framesize

objpoints = np.asarray(camera1objpoints)

(retval, cameraMatrix1, distCoeffs1, cameraMatrix2, distCoeffs2, Rotation, Translation, E, F) = cv2.stereoCalibrate(objpoints, camera1imgpoints, camera2imgpoints, camera1mtx, camera1dist, camera2mtx, camera2dist, camera1framesize, criteria = (cv2.TERM_CRITERIA_MAX_ITER+cv2.TERM_CRITERIA_EPS, 100, 1e-5))

print 'stereo camera'
print cameraMatrix1
print '----------------'
print cameraMatrix2
print '----------------'
print Rotation
print '----------------'
print Translation

(R1, R2, P1, P2, Q, validPixROI1, validPixROI2) = cv2.stereoRectify(cameraMatrix1, distCoeffs1, cameraMatrix2, distCoeffs2, camera1framesize, Rotation, Translation)

print 'rectified'
print '----------------'
print P1
print '----------------'
print P2
print '----------------'

cv2.destroyAllWindows()
