import numpy as np

GREEN_HSV_LOWER = np.array([55, 75, 75], dtype=np.uint8)
GREEN_HSV_UPPER = np.array([75,255,255], dtype=np.uint8)

YELLOW_HSV_LOWER = np.array([30, 75, 75], dtype=np.uint8)
YELLOW_HSV_UPPER = np.array([50, 255, 255], dtype=np.uint8)

RED_HSV_LOWER = np.array([0, 80, 70], dtype=np.uint8)
RED_HSV_UPPER = np.array([20,255,255], dtype=np.uint8)

BLUE_HSV_LOWER = np.array([180, 80, 20], dtype=np.uint8)
BLUE_HSV_UPPER = np.array([260,255,70], dtype=np.uint8)
