#! /usr/bin/env python

import cv2
import numpy
import math
import time

help_message='''
USAGE: sync_detect.py [<video_source>]

Identify the sync blinks of the blue led.
m - Create mask of Region of Interest.
a - Add current mask.
d - Drop current mask.
s - Save region of interest.
c - Clear region of interst.
2 - Reduce zoom factor.
@ (Shift 2) - Double zoom factor.
'''

class App:
    #
    # Initialise the calss structure.
    #
    # source - cv2.VideoCapture instance.
    #
    def __init__ (self, source):
        self.cam = source
        self.led_hu = numpy.loadtxt ('led_hu.txt')
        self.led_hu = self.led_hu [0:4]
        self.led_colour_hsv = numpy.loadtxt ('led_colour.txt').astype(numpy.int)
        self.led_colour_hsv_low = numpy.array (self.led_colour_hsv -
                         [10, 15, 25]).clip (0, 255).astype (numpy.uint8)
        self.led_colour_hsv_high = numpy.array (self.led_colour_hsv +
                         [15, 170, 10]).clip (0, 255).astype (numpy.uint8)
        print ('Colour ', self.led_colour_hsv)
        print ('Low ', self.led_colour_hsv_low)
        print ('High ', self.led_colour_hsv_high)
        hu_norm = math.sqrt (self.led_hu.dot (self.led_hu))
        self.led_hu = self.led_hu / hu_norm
        self.frame_count = 0
        self.mask_points = None
        self.mask_polygons = None
        self.mask_rect = None
        self.mask = None
        self.resized_mask = None
        self.factor = 0.5
        self.mode = 0
        self.MODE_MASK = 1 << 0
        self.MODE_PAUSE = 1 << 1
    #
    # Update the mask data with the supplied polygons.
    #
    # polygons - Vector of points vectors of mask polygons.
    #
    def updateMask (self, polygons):
        if (polygons is None):
            return
        self.mask_rect = None
        self.mask = numpy.zeros ((self.cam.get (cv2.CAP_PROP_FRAME_HEIGHT),
                                  self.cam.get (cv2.CAP_PROP_FRAME_WIDTH)),
                                 dtype = numpy.uint8)
        for points in self.mask_polygons:
            rect = numpy.asarray (cv2.boundingRect (points), dtype=numpy.int32)
            if (self.mask_rect is None):
                self.mask_rect = rect
            else:
                #
                # Compute the union of the new rectangle.
                #
                self.mask_rect [0:2] = numpy.minimum (self.mask_rect [0:2],
                                                      rect [0:2])
                self.mask_rect [2:4] = numpy.maximum (
                    self.mask_rect [0:2] + self.mask_rect [2:4],
                    rect [0:2] + rect [2:4]) - self.mask_rect [0:2]
            self.mask = cv2.fillPoly (self.mask, [ points ], 1)
        self.resized_mask = cv2.resize(self.mask, (0, 0),
                                       fx = self.factor, fy = self.factor)
        self.resized_mask = self.resized_mask.astype (numpy.float) * 0.5 + 0.5
        (height, width) = self.resized_mask.shape
        self.resized_mask = self.resized_mask.repeat (3).reshape (height,
                                                                  width, 3)
        return
    #
    # Reset the mask data in the class instance.
    #
    def resetMask (self):
        self.mask = None
        self.mask_polygons = None
        self.mask_points = None
        self.mask_rect = None
        self.resized_mask = None
    #
    # Append polygon points to polygon.
    #
    def appendMaskPolygon (self, points):
        if (points.shape [0] < 3):
            return (False)
        if (self.mask_polygons is None):
#            self.mask_polygons = self.mask_points.reshape (
#                numpy.append ([1], self.mask_points.shape)
#            )
            self.mask_polygons = [ points ]
            print (self.mask_polygons)
        else:
            print ('Points/Polygons/extended polygons')
            print (points)
            print (self.mask_polygons)
            self.mask_polygons.append (points)
            print (self.mask_polygons)

        self.updateMask (self.mask_polygons)
        return (True)
    #
    # Handle user key feedback.
    #
    # delay - Delay between frames in mSec (-1 don't accept user input).
    #
    def handleKey (self, delay):
        waitDelay = delay
        if (waitDelay < 0):
            waitDelay = 1
        key = 0xFF & cv2.waitKey (waitDelay)
        if (delay < 0):
            return (True)
        #
        # Switch accoding to user key.
        #
        if (key == 27):
            return (False)
        elif (key == ord('q')):
            return (False)
        elif (key == ord ('2')):
            self.factor = self.factor * 2
            self.updateMask (self.mask_polygons)
        elif (key == ord ('@')):
            self.factor = self.factor / 2
            self.updateMask (self.mask_polygons)
        elif (key == ord ('c')):
            self.mask_polygons = None
            self.mask_points = None
            self.updateMask (self.mask_polygons)
        elif (key == ord ('r')):
            self.factor = 1.0
        elif (key == ord ('R')):
            self.mode = 0
            self.resetMask ()
        elif (key == ord ('m')):
            cv2.setMouseCallback ('Monitor', self.handleMouse)
            self.mode = self.mode | self.MODE_MASK
        elif (key == ord ('p')):
            self.mode = self.mode ^ self.MODE_PAUSE
        elif (key == ord (' ')):
            if (self.mode & self.MODE_PAUSE):
                self.mode -= self.MODE_PAUSE
        elif (self.mode & self.MODE_MASK and key == ord ('a')):
            print (self.mask_points)
            if (not self.mask_points is None):
                self.appendMaskPolygon (self.mask_points)
                self.mask_points = None

            self.mode = self.mode - self.MODE_MASK
        elif (self.mode & self.MODE_MASK and key == ord ('d')):
            self.mask_points = None
            self.mode = self.mode - self.MODE_MASK

        return (True)

    def handleMouse (self, event, x, y, flags, param):
        x = int (x/self.factor)
        y = int (y/self.factor)
        if (event == cv2.EVENT_LBUTTONDOWN):
            print ('Location ', x, y)
            if (self.mode & self.MODE_MASK):
                if (self.mask_points is None):
                    self.mask_points = numpy.asarray (
                        (x, y), dtype=numpy.int32).reshape (1,2)
                else:
                    self.mask_points = numpy.vstack ((self.mask_points, (x,y)))
                self.showResizedFrame ('Monitor', self.frame, self.factor)

    def showResizedFrame(self, windowName, frame, factor = 0.5):
        resized = cv2.resize(frame, (0, 0), fx = factor, fy = factor)
        if (self.mode & self.MODE_MASK and not self.mask_points is None):
            for (x, y) in self.mask_points:
                cv2.circle (resized,
                            ((int) (x * self.factor), (int) (y * self.factor)),
                            5, (255, 0, 0), 2)
                
        if (not self.resized_mask is None):
            resized = numpy.multiply (resized, self.resized_mask)
            resized = resized.astype (numpy.uint8)

        text = str(self.frame_count)
        if (self.mode & self.MODE_PAUSE):
            text = text + ' Paused'
        if (self.mode & self.MODE_MASK):
            text = text + ' Mask'
        cv2.putText (resized, text, (30, 30),
                     cv2.FONT_HERSHEY_DUPLEX, 1, (0, 0, 255))
        cv2.imshow(windowName, resized)

    def test_colour (self, bgr, hsv_low, hsv_high):
        #led_colour_hsv = numpy.asarray ([100, 50, 200], dtype=numpy.uint)
        #low = led_colour_hsv*numpy.asarray ([0.9, 0.5, 0.8])
        #low = low.clip (0, 255).astype (numpy.uint8)
        #high = (led_colour_hsv*numpy.asarray ([1.1, 2, 1.3])).clip (0, 255)
        #high = high.astype(numpy.uint8)
        hsv = cv2.cvtColor (bgr.reshape ((1, 1, 3)).astype (numpy.uint8),
                            cv2.COLOR_BGR2HSV).flatten ()
        result = cv2.inRange(hsv,
                             self.led_colour_hsv_low, self.led_colour_hsv_high)
        # print (result.prod () != 0, hsv, result.prod (), result.flatten ())
        # led_colour = numpy.asarray ([255, 242, 230])
        # low = numpy.asarray (led_colour * 0.90, dtype=numpy.uint8)
        # high = numpy.asarray (led_colour * 1.1).clip (0, 255).astype (numpy.uint8)
        # colour = numpy.asarray (bgr, dtype=numpy.uint8).reshape (1, 1, 3)
        # result = cv2.inRange (colour, low, high)
        return (result.prod () != 0)
        # return (True)

    def run (self):

        # Calculate frame delay.
        fps = self.cam.get (cv2.CAP_PROP_FPS)

        if (fps == 0):
            fps = 50

        # Look for potential blinks every quarter second.
        batch = int (fps / 4)
        if (batch <= 0):
            batch = 1

        # Read the first frame.
        _, self.frame = self.cam.read ()
        self.frame_count = self.frame_count + 1
        while True:
            # Read a batch of 4 frames and calculate the difference mask.
            mask_rect = self.mask_rect
            if (mask_rect is None):
                last_frame = self.frame
            else:
                last_frame = self.frame [mask_rect [1] :
                                         mask_rect [1] + self.mask_rect [3],
                                         mask_rect [0] :
                                         mask_rect [0] + self.mask_rect [2]]
            
            diff_mask = numpy.zeros (last_frame.shape [0:2], dtype=numpy.uint8)
            no_batches = batch
            if (self.mode & self.MODE_PAUSE):
                no_batches = 0
            for _ in range(no_batches):
                # Read a new frame
                rc, frame = self.cam.read ()
                self.frame = frame
                if (rc == False):
                    return
                self.frame_count = self.frame_count + 1
                if (not mask_rect is None):
                    frame = frame [mask_rect [1] :
                                   mask_rect [1] + self.mask_rect [3],
                                   mask_rect [0] :
                                   mask_rect [0] + self.mask_rect [2]]
                # Calculate difference from previous frame.
                diff_frame = cv2.absdiff (frame, last_frame)
                diff_frame = cv2.cvtColor (diff_frame, cv2.COLOR_BGR2GRAY)
                _, diff_frame = cv2.threshold (diff_frame, 70, 255,
                                               cv2.THRESH_BINARY)
                # Aggregate differences in this batch.
                diff_mask = diff_mask + diff_frame
                last_frame = frame

            # Make a binary mask.
            diff_mask = diff_mask.clip (0, 1)

            self.showResizedFrame('Monitor', self.frame, self.factor)
            # cv2.imshow ('Diff', diff_mask * 255)
            delay = 1
            if (self.mode & self.MODE_PAUSE):
                delay = 0
            if (not self.handleKey (delay)):
                exit (0)

            if (self.mode & self.MODE_PAUSE):
                continue

            # Search among connected objects.
            _, contours, hier = cv2.findContours (diff_mask, cv2.RETR_EXTERNAL,
                                                  cv2.CHAIN_APPROX_NONE)
            for num in range (len(contours)):
                # Drop small objects.
                if (cv2.contourArea (contours [num]) < 8):
                    continue
                #
                # Draw contour filled shape.
                #
                contour = numpy.zeros (diff_mask.shape, dtype=numpy.uint8)
                shape = cv2.drawContours (contour, contours, num, 1, cv2.FILLED)
                #
                # Test colour.
                # Compute mean colour and standard deviation.
                #
                coloured_shape = cv2.bitwise_and (frame, frame, mask=shape)
                mean_colour = coloured_shape.reshape (-1, 3).sum (0) / shape.sum ()
                std_colour = coloured_shape.reshape (-1, 3).std (0)
                #if (not mask_rect is None):
                #    print ('Colour / STD Colour')
                #    print (mean_colour)
                #    print (std_colour)
                if (self.test_colour (mean_colour,
                                      self.led_colour_hsv_low,
                                      self.led_colour_hsv_high) == False):
                    continue
                #
                # Test shape.
                #
    #                print (' Contour ', frame_count, '/', num,
    #                       ' Colour ', mean_colour.flatten ())
                moments = cv2.moments (shape)
                hu = cv2.HuMoments (moments)
#                print ('Hu')
#                print (moments)
#                print (hu)
                hu_sub = hu [0:self.led_hu.shape [0]]
                hu_norm = math.sqrt (hu_sub.transpose().dot (hu_sub))
                hu_sub = hu_sub / hu_norm
                hu_score = self.led_hu.dot (hu_sub)
                #if (not mask_rect is None):
                #    print ('Hu / Score')
                #    print (hu)
                #    print (hu_score)
                if (hu_score < 0.92):
                    continue
                print ('%d/%d : Colour / Std / Rect '
                       % (self.frame_count, num))
                print (mean_colour)
                print (std_colour)
                print (cv2.boundingRect (contours [num]))
#                print (hu_sub, hu_norm)
#                print (self.hu_shape)
                print (self.led_hu.dot (hu_sub))
                cv2.imshow ('Led shape', coloured_shape)


if __name__ == '__main__':
    import sys
    print help_message
    try:
        fn = sys.argv[1]
    except:
        print '- No video source'
        exit(1)

    # Create capture stream.
    App (cv2.VideoCapture (fn)).run ()

