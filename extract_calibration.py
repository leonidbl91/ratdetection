#! /usr/bin/env python

import cv2
import math
import numpy
import time
import os

help_message='''
USAGE: extract_calibration.py [--scale display_factor] [--outdir <dir>] <camera1 directory> <camera2 directory>
Extracts calibration photos from calibration videos. Pair of videos are
displayed and paused after 2 seconds. User can approve sample and move to next
video pair or forward each one of the videos to a better position.

Keys:
q - Quit
a - Approve and move to next video pair.
r - Reject video pair and move to next.
p - Pause.
1,2,b - Start video forward in camera 1 / 2 / both.
<any key> -  If video is moving foward pauses playback.
'''

class App:
    def __init__ (self, scale = 1, outdir = '.'):
        self.cam1 = None
        self.cam2 = None
        self.fwd1 = False
        self.fwd2 = False
        self.frame_count1 = 0
        self.frame_count2 = 0
        self.frame1 = None
        self.frame2 = None
        self.cam1done = True
        self.cam2done = True
        self.fps = -1
        self.delay = 1000 / 50
        self.frame_limit = -1
        self.serial = -1
        self.scale = scale
        self.outdir = outdir
        print ('Set out dir to '+self.outdir)

    def nextVideo (self):
        return (self.setVideo (self.serial + 1))

    #
    # Set the videos to display.
    # Initialises the cameras and resets the various parameters. Video will run
    # for 2 seconds and then stop for user input.
    #
    # serial - The serial number of the video.
    #
    # Returns: True if successful.
    #          False if serial is invalid.
    #
    # Asserts if captures are invalid.
    #
    def setVideo (self, serial):
        if (serial >= len (self.names1) or serial >= len (self.names2)):
            self.serial = serial
            return (False)

        self.cam1 = cv2.VideoCapture (self.names1 [serial])
        assert self.cam1 != None, \
                'Failed to open camera 1 (%s)' % self.names1 [serial]
            
        self.frame_count1 = 0
        self.frame1 = None
        self.fwd1 = True
        self.cam1done = False
        self.disp_size1 = (
            int (self.cam1.get (cv2.CAP_PROP_FRAME_WIDTH) / self.scale),
            int (self.cam1.get (cv2.CAP_PROP_FRAME_HEIGHT) / self.scale) )

        self.cam2 = cv2.VideoCapture (self.names2 [serial])
        assert self.cam2 != None, \
                'Failed to open camera 2 (%s)' % self.names2 [serial]

        self.frame_count2 = 0
        self.frame2 = None
        self.fwd2 = True
        self.cam2done = False
        self.disp_size2 = (
            int (self.cam2.get (cv2.CAP_PROP_FRAME_WIDTH) / self.scale),
            int (self.cam2.get (cv2.CAP_PROP_FRAME_HEIGHT) / self.scale) )

        # We assume both cameras have the same fps.
        self.fps = self.cam1.get (cv2.CAP_PROP_FPS)
        fps2 = self.cam2.get (cv2.CAP_PROP_FPS)
# TODO: Use same FPS films
#        assert self.fps == fps2, \
#                'Both cameras must have the same FPS (%d != %d)' % \
#                (self.fps, fps2)

        self.delay = 1000 / self.fps
        self.frame_limit = self.fps * 2
        self.serial = serial
        return (True)

    def approveFrames (self):
        print ('Saving in '+self.outdir)
        numpy.save ('%s/calib-cam1-%d' % (self.outdir, self.serial),
                    self.frame1)
        numpy.save ('%s/calib-cam2-%d' % (self.outdir, self.serial),
                    self.frame2)
        return (True)
    #
    # Handle input from the keyboard.
    # key - If greater or equal to 0 assume waitKey is not required and user
    #       input is provided.
    # tdiff - Delay adjustment in seconds (float).
    #         If smaller than 0 assume no delay required.
    #
    # Returns: -1 quit.
    #          0 pause videos wait for user input.
    #          1 continue playback.
    #
    def handle_keyinput (self, key, tdiff):
        if (tdiff < 0):
            pindelay = 0
        else:
            pindelay = int (self.delay - tdiff*1000)
            if (pindelay <= 0):
                pindelay = 1

        if ((key < 0) and (pindelay >= 0)):
            key = 0xFF & cv2.waitKey (pindelay)

        # Escape - Quit.
        if (key == 27):
            return (-1)
        # q - Quit.
        elif (key == ord('q')):
            return (-1)
        # Any other key pauses playback.
        elif (key != 255 and (self.fwd1 or self.fwd2)):
            self.fwd1 = False
            self.fwd2 = False
            self.frame_limit = -1
            return (0)
        # p - Pause.
        elif (key == ord ('p')):
            self.fwd1 = False
            self.fwd2 = False
            self.frame_limit = -1
            return (0)
        # r - Reject.
        elif (key == ord ('r')):
            print ('Rejected photos for %d' % self.serial)
            self.nextVideo ()
        # a - Accept / Approve.
        elif (key == ord ('a')):
            self.approveFrames ()
            print ('Saved photos for %d' % self.serial)
            self.nextVideo ()
        # 1 - Continue camera 1 playback.
        elif (key == ord ('1')):
            self.fwd1 = (not self.cam1done)
        # 2 - Continue camera 2 playback.
        elif (key == ord ('2')):
            self.fwd2 = (not self.cam2done)
        # b - Continue both cameras playback.
        elif (key == ord ('b')):
            self.fwd1 = (not self.cam1done)
            self.fwd2 = (not self.cam2done)

        # Return playback state.
        if (self.fwd1 or self.fwd2):
            return (1)
        else:
            return (0)
    #
    # Run the videos list.
    #
    # videos1 - Ordered list of file names for camera 1.
    # videos2 - Ordered list of file names for camera 2.
    #
    # Number of file names in videos1 & videos2 must be the same.
    #
    def run (self, videos1, videos2):
        assert len (videos1) == len (videos2), \
                'Number of video files from both cameras must match!'
        self.names1 = videos1
        self.names2 = videos2
        tdiff = 0
        #
        # Iterate over user input as long there are more videos.
        #
        while (self.serial < len (videos1)):
            t1 = time.time()
            #
            # If no cameras are available, advance to the next set.
            #
            if (self.cam2done and self.cam1done):
                self.setVideo (self.serial + 1)

            #
            # Advance only if forward is set.
            #
            if (self.fwd1 and not self.cam1done):
                rc, frame = self.cam1.read ()
                if (not rc):
                    self.cam1done = True
                    self.fwd1 = False
                else:
                    self.frame1 = frame
                    self.frame_count1 = self.frame_count1 + 1
                    if (self.scale != 1):
                        frame = cv2.resize (frame, self.disp_size1)
                    cv2.putText (frame, 'Frame %d' % int (self.frame_count1),
                                 (30, 30),
                                 cv2.FONT_HERSHEY_DUPLEX, 1, (0, 0, 255))
                    cv2.imshow ('Camera1', frame)

            if (self.fwd2 and not self.cam2done):
                rc, frame = self.cam2.read ()
                if (not rc):
                    self.cam2done = True
                    self.fwd2 = False
                else:
                    self.frame2 = frame
                    self.frame_count2 = self.frame_count2 + 1
                    if (self.scale != 1):
                        frame = cv2.resize (frame, self.disp_size2)
                    cv2.putText (frame, 'Frame %d' % (self.frame_count2),
                                 (30, 30),
                                 cv2.FONT_HERSHEY_DUPLEX, 1, (0, 0, 255))
                    cv2.imshow ('Camera2', frame)

            #
            # Stop after frame limit frames.
            #
            if (self.frame_limit > 0 and
                ((self.frame_count1 >= self.frame_limit) or
                 (self.frame_count2 >= self.frame_limit))):
                self.frame_limit = -1
                self.fwd1 = False
                self.fwd2 = False

            #
            # If not running, wait forever.
            #
            t2 = time.time ()
            if (self.fwd1 or self.fwd2):
                tdiff = t2 - t1
            else:
                tdiff = -1

            # print (tdiff, self.fwd1, self.fwd2, self.delay)
            rc = self.handle_keyinput (-1, tdiff)
            if (rc < 0):
                break

        cv2.destroyAllWindows ()

if __name__ == '__main__':
    import getopt
    import glob
    import sys

    print help_message

    print (sys.argv[1:])
    opts, args = getopt.getopt (sys.argv [1:], 's:d:', ['scale=', 'outdir='])
    try:
        dir1 = args [0]
        dir2 = args [1]
    except:
        print ('Missing arguments <dir1> <dir2>')
        exit (1)

    print (opts)
    print (args)
    scale = 1
    outdir = '.'
    for opt, arg in opts:
        if (opt in ('-s', '--scale')):
            scale = int (arg)
        if (opt in ('-d', '--outdir')):
            outdir = arg
            if not os.path.isdir(outdir):
                os.mkdir(outdir)
    #
    # Obtain list of files in the directories
    #
    videos1 = sorted (glob.glob (dir1 + '/*.MP4'))
    videos2 = sorted (glob.glob (dir2 + '/*.MP4'))

    App (scale = scale, outdir = outdir).run (videos1, videos2)
