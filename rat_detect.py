import cv2
import numpy as np
from colors import *
import argparse
import json

help_message = """
rat_detect.py <video> <frame to skip>
The following keys may be used while video is running in the monitor window.
2 - Zoom in x2.
@ (Shift 2) - Zoom out x2.
p - Pause.
c - Clear mask.
r - Reset zoom factor.
R - Reset mask & pause mode.
m - Start mask mode (click on image to add a mask polygon).
a - Accept current mask polygon and add mask mode.
d - Clear current mask polygon and end mask mode.
s - Show colour detection binary window (debug purposes).

It is recommended to define a new mask while video is paused.
"""

class App:
    def __init__ (self):
        self.mask_points = None
        self.mask_polygons = None
        self.mask_rect = None
        self.mask = None

        self.mode = 0
        self.MODE_MASK = 1 << 0
        self.MODE_PAUSE = 1 << 1
        self.MODE_DETECT_SHOW = 1 << 2
        self.frame_count = 0

        self.factor = 0.5

        self.detection_lst = []

    #
    # Update the mask data with the supplied polygons.
    #
    # polygons - Vector of points vectors of mask polygons.
    #
    def updateMask (self, polygons):
        if (polygons is None):
            return
        self.mask_rect = None
        self.mask = np.zeros ((self.cap.get (cv2.CAP_PROP_FRAME_HEIGHT),
                               self.cap.get (cv2.CAP_PROP_FRAME_WIDTH)),
                              dtype = np.uint8)
        for points in self.mask_polygons:
            rect = np.asarray (cv2.boundingRect (points), dtype=np.int32)
            if (self.mask_rect is None):
                self.mask_rect = rect
            else:
                #
                # Compute the union of the new rectangle.
                #
                self.mask_rect [0:2] = np.minimum (self.mask_rect [0:2],
                                                   rect [0:2])
                self.mask_rect [2:4] = np.maximum (
                    self.mask_rect [0:2] + self.mask_rect [2:4],
                    rect [0:2] + rect [2:4]) - self.mask_rect [0:2]
            self.mask = cv2.fillPoly (self.mask, [ points ], 1)
        self.resized_mask = cv2.resize(self.mask, (0, 0),
                                       fx = self.factor, fy = self.factor)
        self.resized_mask = self.resized_mask.astype (np.float) * 0.5 + 0.5
        (height, width) = self.resized_mask.shape
        self.resized_mask = self.resized_mask.repeat (3).reshape (height,
                                                                  width, 3)
        return
    #
    # Reset the mask data in the class instance.
    #
    def resetMask (self):
        self.mask = None
        self.mask_polygons = None
        self.mask_points = None
        self.mask_rect = None
        self.resized_mask = None
    #
    # Append polygon points to polygon.
    #
    def appendMaskPolygon (self, points):
        if (points.shape [0] < 3):
            return (False)
        if (self.mask_polygons is None):
#            self.mask_polygons = self.mask_points.reshape (
#                numpy.append ([1], self.mask_points.shape)
#            )
            self.mask_polygons = [ points ]
            print (self.mask_polygons)
        else:
            print ('Points/Polygons/extended polygons')
            print (points)
            print (self.mask_polygons)
            self.mask_polygons.append (points)
            print (self.mask_polygons)

        self.updateMask (self.mask_polygons)
        return (True)

    def handleMouse (self, event, x, y, flags, param):
        x = int (x/self.factor)
        y = int (y/self.factor)
        if (event == cv2.EVENT_LBUTTONDOWN):
            print ('Location ', x, y)
            if (self.mode & self.MODE_MASK):
                if (self.mask_points is None):
                    self.mask_points = np.asarray (
                        (x, y), dtype=np.int32).reshape (1,2)
                else:
                    self.mask_points = np.vstack ((self.mask_points, (x,y)))
                self.showFrame ('FilterImage', self.frame)

    def handleKeyboard (self, delay):
        waitDelay = delay
        if (waitDelay < 0):
            waitDelay = 1
        key = 0xFF & cv2.waitKey (waitDelay)
        if (delay < 0):
            return (True)
        #
        # Switch accoding to user key.
        #
        if (key == 27):
            return (False)
        elif (key == ord('q')):
            return (False)
        elif (key == ord ('2')):
            self.factor = self.factor * 2
            self.updateMask (self.mask_polygons)
        elif (key == ord ('@')):
            self.factor = self.factor / 2
            self.updateMask (self.mask_polygons)
        elif (key == ord ('c')):
            self.mask_polygons = None
            self.mask_points = None
            self.updateMask (self.mask_polygons)
        elif (key == ord ('r')):
            self.factor = 1.0
        elif (key == ord ('R')):
            self.mode = 0
            self.resetMask ()
        elif (key == ord ('m')):
            cv2.setMouseCallback ('FilterImage', self.handleMouse)
            self.mode = self.mode | self.MODE_MASK
            self.showFrame ('FilterImage', self.currentImage)
        elif (key == ord ('p')):
            self.mode = self.mode ^ self.MODE_PAUSE
            self.showFrame ('FilterImage', self.currentImage)
        elif (key == ord ('s')):
            self.mode = self.mode ^ self.MODE_DETECT_SHOW
            self.showFrame ('FilterImage', self.currentImage)
        elif (key == ord (' ')):
            if (self.mode & self.MODE_PAUSE):
                self.mode -= self.MODE_PAUSE
        elif (self.mode & self.MODE_MASK and key == ord ('a')):
            print (self.mask_points)
            if (not self.mask_points is None):
                self.appendMaskPolygon (self.mask_points)
                self.mask_points = None
                self.mode = self.mode - self.MODE_MASK
                print (self.mask)
        elif (self.mode & self.MODE_MASK and key == ord ('d')):
            self.mask_points = None
            self.mode = self.mode - self.MODE_MASK

        return (True)

    def showFrame (self, windowName, frame):
        if (self.factor != 1.0):
            frame = cv2.resize(frame, (0, 0),
                               fx = self.factor, fy = self.factor)

        if (self.mode & self.MODE_MASK and not self.mask_points is None):
            for (x, y) in self.mask_points:
                cv2.circle (frame,
                            ((int) (x * self.factor), (int) (y * self.factor)),
                            5, (255, 0, 0), 2)
                
        #if (not self.resized_mask is None):
        #    frame = numpy.multiply (frame, self.resized_mask)
        #    frame = frame.astype (numpy.uint8)

        text = str(self.frame_count)
        if (self.mode & self.MODE_PAUSE):
            text = text + ' Paused'
        if (self.mode & self.MODE_MASK):
            text = text + ' Mask'
        cv2.putText (frame, text, (30, 30),
                     cv2.FONT_HERSHEY_DUPLEX, 1, (0, 0, 255))
        cv2.imshow(windowName, frame)

    def find_connected_components(self, img):
        """Find the connected components in img being a binary image.
        it approximates by rectangles and returns its centers
        """

        _, contours, hierarchy  = cv2.findContours(img.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        contours = sorted(contours, key = cv2.contourArea, reverse = True)[:10]
        rects = []
        img = cv2.drawContours(img,contours,-1,(0,255,0),3)
        for contour in contours:
            # Approximates rectangles
            bound_rect = cv2.boundingRect(np.array(map(lambda x: x[0], contour)))
            rects.append(bound_rect)
     
        return rects

    def find_maximal_rect(self, rects):
        if len(rects) == 0:
            return None
        return max(rects, key=lambda rec: rec[2]*rec[3]) 

    def img_threshold(self, img, lower, upper):
        filterImg = cv2.inRange(img, lower, upper)
        return filterImg

    def draw_detection(self, frame, max_rect):
        if not max_rect is None:
            # add center of mass
            filterBitmap = np.zeros ((frame.shape [0], frame.shape [1]),
                                     dtype = np.float)
            radius = int ((max_rect[2]**2 + max_rect[3]**2) ** 0.5)
            cv2.circle (filterBitmap, (max_rect[0], max_rect[1]),
                        radius + 4, 0.75, -1)
            filterBitmap = filterBitmap + 0.25
            filterBitmap = filterBitmap.repeat (3).reshape (filterBitmap.shape [0],
                                                        filterBitmap.shape [1],
                                                        3)
            res = (frame * filterBitmap).astype (np.uint8)
        else:
            res = frame / 4

        self.currentImage = res
        self.showFrame ('FilterImage', res)

    def color_detect(self, frame, lower_hsv, upper_hsv):
        """
        Method returns(x,y,w,h) of the wanted color.
        if no detection, None is returned.
        """ 
        # Convert BGR to HSV
        hsv_image = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

        # Threshold the HSV image to get only green colors
        filterImg = self.img_threshold(hsv_image, lower_hsv, upper_hsv)
        if (self.mode & self.MODE_DETECT_SHOW):
            self.showFrame ("ColourDetect", filterImg)
        rects = self.find_connected_components(filterImg)
        max_rect = self.find_maximal_rect(rects)
    
        if not max_rect is None and max_rect[2] * max_rect[3] > 4:
            return max_rect
        else:
            return None
     
    def run_detection(self, infile, outfile, color_lower, color_upper,
                      frame_to_skip):
        print "Loading file to detect: ", infile
        self.cap = cv2.VideoCapture(infile)

        skipped = 0
        print 'Skipping frames'
        while skipped < frame_to_skip:
            self.cap.read()
            skipped = skipped+1
        
        print 'Done'

        while(1):
    
            if (not self.handleKeyboard (5)):
                break

            if (self.mode & self.MODE_PAUSE):
                continue
            # Take each frame
            _, self.frame = self.cap.read()
            if self.frame is None:
                break

            self.frame_count = self.frame_count + 1
            # self.frame = cv2.resize(self.frame, (800, 600)) 

            #color detection

            if (not self.mask is None):
                frame = cv2.bitwise_and (self.frame, self.frame,
                                         mask = self.mask)
            else:
                frame = self.frame
            max_rect = self.color_detect(frame, color_lower, color_upper)
             
            if (not max_rect is None):
                print "detected: ", self.frame_count, max_rect
            self.draw_detection(frame, max_rect)
            self.detection_lst.append(max_rect)

        cv2.destroyAllWindows()
        with open(outfile, "w") as f:
            def transform_detection_rect(rect):
                if rect is None: return None
                x,y,w,h = rect
                return (x + w//2 , y+ h//2)
            json.dump(map(transform_detection_rect, self.detection_lst), f)
        print "Output file: %s is written succesfully" % (outfile,)

def parse_args():
    parser = argparse.ArgumentParser(description='Rat Detection')
    parser.add_argument('--input', dest='input', required=True, help='Input Video File')
    parser.add_argument('--skip', type=int, dest='skip', required=True, help='Frames to skip')
    parser.add_argument('--out', dest='out', required=True, help='Output File')

    args = parser.parse_args()
    return args

if __name__ == "__main__":
    args = parse_args()
    # App ().run_detection(args.input, args.out, YELLOW_HSV_LOWER, YELLOW_HSV_UPPER, args.skip)
    # App ().run_detection(args.input, args.out, GREEN_HSV_LOWER, GREEN_HSV_UPPER, args.skip)
    App ().run_detection(args.input, args.out, RED_HSV_LOWER, RED_HSV_UPPER, args.skip)
