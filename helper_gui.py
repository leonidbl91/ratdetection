#! /usr/bin/env python

import cv2
import numpy
import math
import time

help_message='''
USAGE: helper_gui.py [<video_source>]

Keys:
q - Quit
p - Pause
c - Find connected elements in all and print Hu Moments.
C - Clear diff accumulator.
f - Fast forward.
r - Regular play.
s - Slow motion.
Left button click - Print selected colour.
'''

class App:
    def __init__ (self, source, scale = 0.5):
        self.cam = source
        self.factor = scale

        self.mask_points = None
        self.mask_polygons = None
        self.mask_rect = None
        self.mask = None
        self.resized_mask = None

        self.mode = 0

        self.MODE_MASK = 1 << 0
        self.MODE_PAUSE = 1 << 1


    def showFrame (self, windowName, frame, factor = 0.5):
        if (self.factor != 1.0):
            frame = cv2.resize(frame, (0, 0),
                               fx = self.factor, fy = self.factor)

        if (self.mode & self.MODE_MASK and not self.mask_points is None):
            for (x, y) in self.mask_points:
                cv2.circle (frame,
                            ((int) (x * self.factor), (int) (y * self.factor)),
                            5, (255, 0, 0), 2)
                
        if (not self.resized_mask is None):
            frame = numpy.multiply (frame, self.resized_mask)
            frame = frame.astype (numpy.uint8)

        text = str(self.frame_count)
        if (self.mode & self.MODE_PAUSE):
            text = text + ' Paused'
        if (self.mode & self.MODE_MASK):
            text = text + ' Mask'
        cv2.putText (frame, text, (30, 30),
                     cv2.FONT_HERSHEY_DUPLEX, 1, (0, 0, 255))
        cv2.imshow(windowName, frame)
#        if (self.factor != 1.0):
#            frame = cv2.resize(frame, (0, 0),
#                               fx = self.factor, fy = self.factor)
#        if (not self.mask is None):
#            frame = numpy.multiply (resized, self.mask)
#        cv2.imshow(windowName, resized)

#    def setmask (self, mask):
#        if (mask is None):
#            self.mask = None
#            return
#
#        print (mask)
#        self.mask = numpy.zeros ((self.cam.get (cv2.CAP_PROP_FRAME_HEIGHT),
#                                  self.cam.get (cv2.CAP_PROP_FRAME_WIDTH)),
#                                 dtype = numpy.uint8)
#        self.mask = cv2.fillPoly (self.mask, mask, 1)
#        self.mask = self.mask.astype (numpy.float)*0.5 + 0.5
#        print (self.mask)
#        self.mask = self.mask.repeat (3).reshape (
#            self.cam.get (cv2.CAP_PROP_FRAME_HEIGHT),
#            self.cam.get (cv2.CAP_PROP_FRAME_WIDTH),
#            3)
#        return
    #
    # Update the mask data with the supplied polygons.
    #
    # polygons - Vector of points vectors of mask polygons.
    #
    def updateMask (self, polygons):
        if (polygons is None):
            return
        self.mask_rect = None
        self.mask = numpy.zeros ((self.cam.get (cv2.CAP_PROP_FRAME_HEIGHT),
                                  self.cam.get (cv2.CAP_PROP_FRAME_WIDTH)),
                                 dtype = numpy.uint8)
        for points in self.mask_polygons:
            rect = numpy.asarray (cv2.boundingRect (points), dtype=numpy.int32)
            if (self.mask_rect is None):
                self.mask_rect = rect
            else:
                #
                # Compute the union of the new rectangle.
                #
                self.mask_rect [0:2] = numpy.minimum (self.mask_rect [0:2],
                                                      rect [0:2])
                self.mask_rect [2:4] = numpy.maximum (
                    self.mask_rect [0:2] + self.mask_rect [2:4],
                    rect [0:2] + rect [2:4]) - self.mask_rect [0:2]
            self.mask = cv2.fillPoly (self.mask, [ points ], 1)
        self.resized_mask = cv2.resize(self.mask, (0, 0),
                                       fx = self.factor, fy = self.factor)
        self.resized_mask = self.resized_mask.astype (numpy.float) * 0.5 + 0.5
        (height, width) = self.resized_mask.shape
        self.resized_mask = self.resized_mask.repeat (3).reshape (height,
                                                                  width, 3)
        return
    #
    # Reset the mask data in the class instance.
    #
    def resetMask (self):
        self.mask = None
        self.mask_polygons = None
        self.mask_points = None
        self.mask_rect = None
        self.resized_mask = None
    #
    # Append polygon points to polygon.
    #
    def appendMaskPolygon (self, points):
        if (points.shape [0] < 3):
            return (False)
        if (self.mask_polygons is None):
#            self.mask_polygons = self.mask_points.reshape (
#                numpy.append ([1], self.mask_points.shape)
#            )
            self.mask_polygons = [ points ]
            print (self.mask_polygons)
        else:
            print ('Points/Polygons/extended polygons')
            print (points)
            print (self.mask_polygons)
            self.mask_polygons.append (points)
            print (self.mask_polygons)

        self.updateMask (self.mask_polygons)
        return (True)

    def handleMouse (self, event, x, y, flags, param):
        x = int (x/self.factor)
        y = int (y/self.factor)
        if (event == cv2.EVENT_LBUTTONDOWN):
            print ('Location ', x, y)
            if (self.mode & self.MODE_MASK):
                if (self.mask_points is None):
                    self.mask_points = numpy.asarray (
                        (x, y), dtype=numpy.int32).reshape (1,2)
                else:
                    self.mask_points = numpy.vstack ((self.mask_points, (x,y)))
                self.showFrame ('Video', self.frame)
            else:
                colour = self.frame[y, x]
                print ('BGR ',colour)
                bgr_colour = numpy.ndarray ([1, 1, 3], numpy.dtype ('uint8'))
                bgr_colour [0, 0] = colour
                hsv_colour = cv2.cvtColor (bgr_colour, cv2.COLOR_BGR2HSV)
                print ('HSV ', hsv_colour [0, 0])
                painted_frame = self.frame
                cv2.circle (painted_frame, (x, y), 5, (255, 0, 0), 2)
                print ('Location ', x, y)
                self.showFrame ('Video', painted_frame)

#    def get_colour (self, event, x, y, flags, param):
#        x = int (x/self.factor)
#        y = int (y/self.factor)
#        if (event == cv2.EVENT_LBUTTONDOWN):
#            colour = self.frame[y, x]
#            print ('BGR ',colour)
#            bgr_colour = numpy.ndarray ([1, 1, 3], numpy.dtype ('uint8'))
#            bgr_colour [0, 0] = colour
#            hsv_colour = cv2.cvtColor (bgr_colour, cv2.COLOR_BGR2HSV)
#            print ('HSV ', hsv_colour [0, 0])
#            painted_frame = self.frame
#            cv2.circle (painted_frame, (x, y), 5, (255, 0, 0), 2)
#            print ('Location ', x, y)
#            self.showResizedFrame ('Video', painted_frame, self.factor)

    def draw_connected (self):
        frame_diff = cv2.bitwise_and (self.frame, self.frame,
                                      mask=self.all_frame_diff)
        frame_diff = cv2.bitwise_and (frame_diff, frame_diff,
                                      mask=self.mask)
        contours = cv2.findContours (frame_diff,
                                     cv2.RETR_FLOODFILL, cv2.CHAIN_APPROX_NONE)
        print ('Connected # of contours ', len (contours [1]))
        print (type (contours))
        for num in range (len (contours [1])):
            if (cv2.contourArea (contours [1] [num]) < 10):
                continue
            print ("Contour ", num)
            contour = numpy.zeros (self.all_frame_diff.shape, dtype=numpy.uint8)
            shape = cv2.drawContours (contour, contours [1], num, 255, cv2.FILLED)
            moments = cv2.moments (shape)
            hu = cv2.HuMoments (moments)

            # Test colour.
            coloured_shape = cv2.bitwise_and (self.frame, self.frame,
                                              mask=shape)
            mean_colour = (coloured_shape.reshape (-1, 3).sum (0)
                           / (shape.sum () / 255))
            std_colour = coloured_shape.reshape (-1, 3).std (0)

            print ('Hu Moments')
            print (hu)
            print ('Bounding Rectangle ', cv2.boundingRect (contours [1] [num]))
            print ('Colour ', mean_colour.flatten (), std_colour.flatten ())
            print ('Area ',cv2.contourArea (contours [1] [num]))
            cv2.imshow ('Contour', shape)
            key = cv2.waitKey (2000)
            if (key == 27):
                break
            elif (key == ord ('q')):
                break

    def handle_keyinput (self, key, tdiff):
        if (tdiff < 0):
            pindelay = 0
        else:
            pindelay = int (self.delay - tdiff*1000)
            if (pindelay <= 0):
                pindelay = 1

        if (pindelay >= 0):
            key = 0xFF & cv2.waitKey (pindelay)
        if (key == 27):
            return (False)
        elif (key == ord('q')):
            return (False)
        elif (key == ord ('p')):
            self.mode = self.mode ^ self.MODE_PAUSE
            self.showFrame ('Video', self.frame)
            return (True)
        elif (key == ord ('c')):
            self.mode = self.mode ^ self.MODE_PAUSE
            self.draw_connected ()
            return (True)
        elif (key == ord ('C')):
            self.all_frame_diff = numpy.zeros (self.frame.shape [0:2],
                                               dtype=numpy.uint8)
        elif (key == ord ('f')):
            self.delay = int (self.delay / 4)
            if (self.delay <= 0):
                self.delay = 1
        elif (key == ord ('s')):
            self.delay = int (self.basic_delay * 2)
        elif (key == ord ('r')):
            self.delay = self.basic_delay
        elif (key == ord ('R')):
            self.mode = 0
            self.resetMask ()
        elif (key == ord ('m')):
            cv2.setMouseCallback ('Video', self.handleMouse)
            self.mode = self.mode | self.MODE_MASK
            self.showFrame ('Video', self.frame)
        elif (key == ord ('2')):
            self.factor = self.factor * 2
            self.updateMask (self.mask_polygons)
        elif (key == ord ('@')):
            self.factor = self.factor / 2
            self.updateMask (self.mask_polygons)
        elif (key == ord ('=')):
            self.factor = 1.0
            self.updateMask (self.mask_polygons)
        elif (key == ord (' ')):
            if (self.mode & self.MODE_PAUSE):
                self.mode -= self.MODE_PAUSE
        elif (self.mode & self.MODE_MASK and key == ord ('a')):
            print (self.mask_points)
            if (not self.mask_points is None):
                self.appendMaskPolygon (self.mask_points)
                self.mask_points = None
                self.mode = self.mode - self.MODE_MASK
                print (self.mask)
        elif (self.mode & self.MODE_MASK and key == ord ('d')):
            self.mask_points = None
            self.mode = self.mode - self.MODE_MASK

        return (True)

    def run (self):
        # Calculate frame delay.
        fps = self.cam.get (cv2.CAP_PROP_FPS)

        if (fps == 0):
            fps = 50
        self.basic_delay = int (math.floor (1000 / fps))
        self.delay = self.basic_delay

        # Obtain first frame
        rc, self.frame = self.cam.read ()
        print (self.frame.shape)
        last_frame = self.frame
        self.frame_count = 1

        # Set window
        cv2.namedWindow ('Video')
        cv2.setMouseCallback ('Video', self.handleMouse)

        self.all_frame_diff = numpy.zeros (last_frame.shape [0:2],
                                           dtype=numpy.uint8)
        rc, self.frame = self.cam.read ()
        tdiff = 0
        # Display video
        while True:
            if (not self.handle_keyinput (0, tdiff)):
                break

            if (self.mode & self.MODE_PAUSE):
                continue

            last_frame = self.frame
            t1 = time.time()
            rc, self.frame = self.cam.read ()
            self.frame_count = self.frame_count + 1
            if (self.frame_count % 10 == 0):
                print ('Frame count ', self.frame_count)

            frame_diff = cv2.absdiff (self.frame, last_frame)
            frame_diff = cv2.cvtColor (frame_diff, cv2.COLOR_BGR2GRAY)
            rc, frame_mask = cv2.threshold (frame_diff, 70, 255, cv2.THRESH_BINARY);
            frame_diff = cv2.bitwise_and (self.frame, self.frame,
                                          mask=frame_mask)
            self.all_frame_diff = (self.all_frame_diff +
                                   frame_mask).clip (0, 255)


            self.showFrame('Video', self.frame)
    #        cv2.imshow ('Diff', frame_diff)
    #        cv2.imshow ('AllDiff', all_frame_diff)

            t2 = time.time ()
            tdiff = t2 - t1

        cv2.destroyAllWindows ()

if __name__ == '__main__':
    import sys
    import getopt

    print help_message

    scale = 0.5
    opts, args = getopt.getopt (sys.argv [1:], 's:', ['scale=', 'poly='])
    try:
        fn = args [0]
    except:
        print ('Missing argument <video file>')
        exit (1)

    poly_points = None
    for opt, arg in opts:
        if (opt in ('-s', '--scale')):
            scale = int (arg)
        if (opt in ('--poly')):
            poly_points = numpy.loadtxt (arg)
            poly_points = numpy.asarray ([poly_points], dtype=numpy.int32)
            print (poly_points)

    # Create capture stream.
    app = App (cv2.VideoCapture (fn), scale)
    app.updateMask (poly_points)
    app.run ()

    
#        ch = 0xFF & cv2.waitKey (pindelay)
#        if (ch == 27):
#            break
#        if (ch == ord ('q')):
#            break
#        if (ch == ord ('p')):
#            cv2.waitKey (0)

