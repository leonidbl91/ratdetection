import argparse
import json
import numpy as np

def parse_args():
    parser = argparse.ArgumentParser(description='Rat Detection')
    parser.add_argument('--input1', dest='input1', required=True, help='Input First color detection file')
    parser.add_argument('--input2', dest='input2', required=True, help='Input Second color detection file')
    parser.add_argument('--output1', dest='output1', required=True, help='Output First color fixed detection file')
    parser.add_argument('--output2', dest='output2', required=True, help='Output Second color fixed detection file')

    args = parser.parse_args()
    return args

def fix_lsts(lst1, lst2):
    result1 = []
    result2 = []
    l = min(len(lst1), len(lst2))
    print "Fixing %d frames" % (l, )
    diff_vec = np.array([0, 0])
    for i in xrange(l):
        if (not lst1[i] is None) and (not lst2[i] is None):
            e1 = np.array(lst1[i])
            e2 = np.array(lst2[i])
            diff_vec = e1 - e2
        elif not lst1[i] is None:
            e1 = np.array(lst1[i])
            e2 = e1 - diff_vec
        elif not lst2[i] is None:
            e2 = np.array(lst2[i])
            e1 = e2 + diff_vec
        else:
            e1 = np.array(None)
            e2 = np.array(None)
        result1.append(e1.tolist())
        result2.append(e2.tolist())
            
    return result1, result2



def main():
    args = parse_args()
    l1 = json.load(open(args.input1))
    l2 = json.load(open(args.input2))
    fix_l1, fix_l2 = fix_lsts(l1, l2)
    json.dump(fix_l1, open(args.output1, "w"))
    json.dump(fix_l2, open(args.output2, "w"))


if __name__ == "__main__":
    main()
