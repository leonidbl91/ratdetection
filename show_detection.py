import argparse
import json
import cv2
import numpy as np

def parse_args():
    parser = argparse.ArgumentParser(description='Rat Detection')
    parser.add_argument('--video', dest='video', required=True, help='Video file')
    parser.add_argument('--detection', dest='detection', required=True, help='Detection file')
    parser.add_argument('--skip', dest='skip', type=int, required=True, help='Frames to skip')
    args = parser.parse_args()
    return args


def show_frame(frame, point, radius):
    # add center of mass
    filterBitmap = np.zeros ((frame.shape [0], frame.shape [1]),
			     dtype = np.float)
    if not point is None:
        cv2.circle (filterBitmap, tuple(point),
	    	    radius + 4, 0.75, -1)
    filterBitmap = filterBitmap + 0.25

    filterBitmap = filterBitmap.repeat (3).reshape (filterBitmap.shape [0],
    						filterBitmap.shape [1],
    						3)
    res = (frame * filterBitmap).astype (np.uint8)
    RESIZE_FACTOR = 0.5
    res = cv2.resize(res, (0,0), fx = RESIZE_FACTOR, fy = RESIZE_FACTOR)
    cv2.imshow("detection", res)


def main():
    args = parse_args()
    CIRCLE_SIZE = 20
    detect_lst = json.load(open(args.detection))
    cap = cv2.VideoCapture(args.video)
    
    skipped = 0
    print 'Skipping frames'
    while skipped < args.skip:
        cap.read()
        skipped = skipped+1
    
    print 'Done'
    for d in detect_lst:
        _, frame = cap.read()
        show_frame(frame, d, CIRCLE_SIZE)
        if cv2.waitKey(1) & 0xFF == 27:
            break
    cv2.destroyAllWindows()


if __name__ == "__main__":
   main()
