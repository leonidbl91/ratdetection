import cv2
import sys

usage = 'trim_movie.py <movie> <from_frame> <output>'

class Trimmer:
    def __init__(self, film, output):
        self.capture = cv2.VideoCapture(film)
        self.output = cv2.VideoWriter(output, int(self.capture.get(cv2.CAP_PROP_FOURCC)), 
                                      int(self.capture.get(cv2.CAP_PROP_FPS)), 
                                      (int(self.capture.get(cv2.CAP_PROP_FRAME_WIDTH)), 
                                          int(self.capture.get(cv2.CAP_PROP_FRAME_HEIGHT))))

    def trim(self, frame):
        counter = 0
        while counter < frame:
            self.capture.read()
            counter = counter + 1

        while True:
            image = self.capture.read()
            self.output.write(image)

if __name__ == '__main__':
    print usage
    try:
        film = sys.argv[1]
    except:
        print 'No video file'
        exit(1)

    try:
        frame = sys.argv[2]
    except:
        print 'No frame'
        exit(1)

    try:
        output = sys.argv[3]
    except:
        print 'No output file'
        exit(1)

    Trimmer(film, output).trim(frame)
