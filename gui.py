import Tkinter, Tkconstants, tkFileDialog
from Tkinter import *
from rat_detect import run_detection

class TkFileDialogExample(Tkinter.Frame):

  def __init__(self, root):

    Tkinter.Frame.__init__(self, root)
   

    # options for buttons
    button_opt = {'fill': Tkconstants.BOTH, 'padx': 5, 'pady': 5}

    v = IntVar()
    var1 = IntVar()
    var2 = IntVar()
    var3 = IntVar()
    var4 = IntVar()
    color = 0


    def callback(number):
      color = number
      if (color is 1 and var1.get() is 1):
        Tkinter.var= "Red"
        print Tkinter.var
      elif (color is 2  and var2.get() is 1):
        Tkinter.var= "Green"
        print Tkinter.var
      elif (color is 3  and var3.get() is 1):
        Tkinter.var= "Yellow"
        print Tkinter.var 
      elif (color is 4  and var4.get() is 1):
        Tkinter.var= "Blue"  
        print Tkinter.var           

  
   # Tkinter.Radiobutton(self, text="Red", variable=v, value=1, command=lambda: callback(1)).pack(anchor=W)  
   # Tkinter.Radiobutton(self, text="Green", variable=v, value=2, command=lambda: callback(2)).pack(anchor=W)
   # Tkinter.Radiobutton(self, text="Yellow", variable=v, value=3, command=lambda: callback(3)).pack(anchor=W)
    
    label1 = Label(root, text="pick detection color:",  fg="gray").pack(anchor=W)
    
    Tkinter.Checkbutton(self, text=" Red ",variable=var1, fg="red", command=lambda: callback(1)).pack()
    Tkinter.Checkbutton(self, text="Green",variable=var2, fg="green", command=lambda: callback(2)).pack()
    Tkinter.Checkbutton(self, text="Yellow",variable=var3,  fg="yellow", command=lambda: callback(3)).pack()
    Tkinter.Checkbutton(self, text="Blue",variable=var4,  fg="blue", command=lambda: callback(4)).pack()

    # define buttons
    Tkinter.Button(self, text='open video 1', command=self.askopenfile_video1).pack(**button_opt)
    Tkinter.Button(self, text='open calibration image 1', command=self.askopenfile_image1).pack(**button_opt)
    Tkinter.Button(self, text='open video 2', command=self.askopenfile_video2).pack(**button_opt)
    Tkinter.Button(self, text='open calibration image 2', command=self.askopenfile_image2).pack(**button_opt)
    Tkinter.Button(self, text='START',font = "Verdana 10 bold", command=self.start).pack(**button_opt)
    Tkinter.Button(self, text='save result as...', command=self.asksaveasfile).pack(**button_opt)

    # define options for opening or saving a file
    self.file_opt_video = options = {}
    options['defaultextension'] = '.txt'
    options['filetypes'] = [('all files', '.*'), ('video file', '.mp4'), ('video file', '.avi'), ('video file', '.wmv')]
    options['initialdir'] = 'C:\\'   
    options['initialfile'] = 'myfile.txt'
    options['parent'] = root
    options['title'] = 'GUI'
      
    # define options for opening or saving a file
    self.file_opt_image = options = {}
    options['defaultextension'] = '.txt'
    options['filetypes'] = [('all files', '.*'), ('image files', '.jpg'), ('image files', '.png')]
    options['initialdir'] = 'C:\\'
    options['initialfile'] = 'myfile.txt'
    options['parent'] = root
    options['title'] = 'GUI'
    

    # This is only available on the Macintosh, and only when Navigation Services are installed.
    #options['message'] = 'message'

    # if you use the multiple file version of the module functions this option is set automatically.
    #options['multiple'] = 1

    # defining options for opening a directory
    self.dir_opt = options = {}
    options['initialdir'] = 'C:\\'
    options['mustexist'] = False
    options['parent'] = root
    options['title'] = 'This is a title'

  def askopenfile_video1(self):
    """Returns an opened file in read mode."""
    self.pathvideo1 = tkFileDialog.askopenfilename(**self.file_opt_video)
  def askopenfile_video2(self):
    """Returns an opened file in read mode."""
    self.pathvideo2 = tkFileDialog.askopenfilename(**self.file_opt_video)


  def askopenfile_image1(self):
    """Returns an opened file in read mode."""
    self.pathimage1 = tkFileDialog.askopenfilename(**self.dir_opt_image)
  def askopenfile_image2(self):
    """Returns an opened file in read mode."""
    self.pathimage2 = tkFileDialog.askopenfilename(**self.dir_opt_image)


  def asksaveasfile(self):
    """Returns an opened file in write mode."""
    #self.save_path = tkFileDialog.asksaveasfilename(mode='r', **self.file_opt_image)
    self.save_path = tkFileDialog.asksaveasfile(**self.file_opt_image)
    #return tkFileDialog.asksaveasfile(mode='w', **self.file_opt)
    
  def start(self):
    print 'start'
    # starting run
    run_detection(self.pathvideo1, "out1")
    run_detection(self.pathvideo2, "out2")
    quit()    
 
  def quit(self):
    root.quit()    
  
    

if __name__=='__main__':
  root = Tkinter.Tk()
  TkFileDialogExample(root).pack()
  root.title("3D")
  root.mainloop()
  
  
  
  #####################################################3
